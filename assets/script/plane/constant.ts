import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

export class constant {
    public static enemy_type =
        {
            TYPE1: 1,
            TYPE2: 2,
        };

    public static enemy_combination =
        {
            PLAN1: 1,
            PLAN2: 2,
            PLAN3: 3,
        };

    public static level =
        {
            LEVEL1: 1,
            LEVEL2: 2,
            LEVEL3: 3,
        }
}